﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProgressBar : MonoBehaviour {

    float maxSize = 0f;
    float currentSize = 0f;
    public float increment = 0.1f;
    public float percent = 0f;

    Transform bar;

	// Use this for initialization
	void Start ()
    {
        bar = GetComponent<Transform>();
        maxSize = bar.localScale.y;
	}
	
	// Update is called once per frame
	void Update ()
    {
        currentSize = maxSize * percent;

        bar.localScale = new Vector2(bar.localScale.x, currentSize);
	}

    public void Increase ()
    {
        if (percent < 1.0f) percent += increment*Time.deltaTime;
    }

    public void Decrease ()
    {
        if (percent > 0f) percent -= increment * Time.deltaTime;
    }

    void Caught ()
    {
        
    }
}
