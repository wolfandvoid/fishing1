﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fish : MonoBehaviour {

    public float minY, maxY;

    Vector2 target;

    Transform t;

	// Use this for initialization
	void Start ()
    {
        t = transform;

        Invoke("MoveToNewPosition", 1f);
	}
	
	// Update is called once per frame
	void Update ()
    {
        t.position = Vector2.MoveTowards(transform.position, target, Time.deltaTime);
	}

    private void MoveToNewPosition()
    {
        target = GetNewPos();

        Invoke("MoveToNewPosition", Random.Range(1f, 5f));
    }

    Vector2 GetNewPos ()
    {
        return new Vector2(0, Random.Range(minY, maxY));
    }
}
