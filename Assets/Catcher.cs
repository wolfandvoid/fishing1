﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Catcher : MonoBehaviour {

    public ProgressBar bar;
    public float gravity = 10f;
    public float force = 10f;

    private Rigidbody2D rb;

    private bool catching = false;

	// Use this for initialization
	void Start ()
    {
        rb = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        rb.AddForce(new Vector2(0, -gravity));

        Controls();

        if (catching) bar.Increase();
        else bar.Decrease();

        catching = false;
	}

    void Controls ()
    {
        if (Input.GetKey(KeyCode.UpArrow))
        {
            rb.AddRelativeForce(new Vector2(0, force));
        }
    }

    private void OnTriggerStay2D(Collider2D col)
    {
        if (col.gameObject.name == "Fish")
        {
            catching = true;    
        }
    }
}
